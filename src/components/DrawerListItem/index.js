import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import WindowIcon from "@mui/icons-material/Window";
import { Box } from "@mui/material";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import SettingsIcon from "@mui/icons-material/Settings";
import LogoutIcon from "@mui/icons-material/Logout";
import GroupsOutlinedIcon from "@mui/icons-material/GroupsOutlined";
const DrawerListItem = ({ open }) => {
	return (
		<>
			<Box>
				<List component="nav">
					<ListItem
						// onClick={(e) => setType("dashboard")}
						style={{
							backgroundColor: "white",
						}}
					>
						<ListItemIcon>
							<WindowIcon />
						</ListItemIcon>
						<ListItemText primary={"dashboard"} />
					</ListItem>
					<ListItem
						// onClick={(e) => setType("upload")}
						style={{ backgroundColor: "white" }}
					>
						<ListItemIcon>
							<MenuBookIcon />
						</ListItemIcon>
						<ListItemText primary={"Content Upload"} />
					</ListItem>
					<ListItem
						// onClick={(e) => setType("upload")}
						style={{ backgroundColor: "white" }}
					>
						<ListItemIcon>
							<GroupsOutlinedIcon />
						</ListItemIcon>
						<ListItemText primary={"Content Upload"} />
					</ListItem>
					<ListItem
						// onClick={(e) => setType("upload")}
						style={{ backgroundColor: "white" }}
					>
						<ListItemIcon>
							<GroupsOutlinedIcon />
						</ListItemIcon>
						<ListItemText primary={"Content Upload"} />
					</ListItem>
				</List>
			</Box>
			<Box
				sx={{ flexGrow: 1 }}
				style={{ display: "flex", flexDirection: "column-reverse" }}
			>
				<List component="nav">
					<ListItem
						// onClick={(e) => setType("settings")}
						style={{
							backgroundColor: "white",
						}}
					>
						<ListItemIcon>
							<SettingsIcon />
						</ListItemIcon>
						<ListItemText primary={"Settings"} />
					</ListItem>
					<ListItem
						// onClick={(e) => setType("login")}
						style={{ backgroundColor: "white" }}
					>
						<ListItemIcon>
							<LogoutIcon />
						</ListItemIcon>
						<ListItemText primary={"Exit"} />
					</ListItem>
				</List>
			</Box>
		</>
	);
};
export default DrawerListItem;
